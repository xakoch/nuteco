$(document).ready(function() {

  // Smooth Scrolling
  $('.menu li a, a.arrow[href^="#"]').click(function(e) {
    e.preventDefault();
    var target = $(this).attr('href');
    if (target == '#') return;
    $('body, html').animate({
      scrollTop: $(target).offset().top - 50
    }, 1000);
  });

  // product - slider
  $(".product-slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 300,
    arrows: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1100,
      settings: {
        slidesToShow: 3,
      }
    }, {
      breakpoint: 950,
      settings: {
        slidesToShow: 2,
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
      }
    }]
  });

  // partner - slider
  $(".partner-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    speed: 500,
    arrows: true,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 2500,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
      }
    }, {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      }
    }]
  });

  // Mobile menu
  $('.mobile-btn').click(function(e){
    e.preventDefault();
    $('.menu').slideToggle();
  });

  $(window).scroll(function(){
    var nav = $('header');
    if ($(this).scrollTop() > 20 ) {
      nav.addClass('fixed');
    } else {
      nav.removeClass('fixed');
    }
  });

  // FAQ
  $('.faq-text').hide();
  $(".faq-title").click( function () {
    var container = $(this).parents(".faq");
        answer = container.find(".faq-text");
        trigger = container.find(".faq-t");
    answer.slideToggle(300);
    if (trigger.hasClass("faq-o")) {
      trigger.removeClass("faq-o");
    }
    else {
      trigger.addClass("faq-o");
    }
  });

  // num
  $('[name="phone"]').mask("+998(11)111-1111");

  // Отправка заявки
  $('#form').submit(function() {
    $.ajax({
      type: "POST",
      url: "../mail.php",
      data: $(this).serialize()
    }).done(function() {
      $('.js-overlay-thank-you').fadeIn();
      $(this).find('input').val('');
      $('#form').trigger('reset');
    });
    return false;
  });

  // Закрыть попап «спасибо»
  $('.js-close-thank-you').click(function() {
    $('.js-overlay-thank-you').fadeOut();
    $('.remodal-close').trigger('click');
  });

  $(document).mouseup(function (e) {
    var popup = $('.popup');
    if (e.target!=popup[0]&&popup.has(e.target).length === 0){
      $('.js-overlay-thank-you').fadeOut();
    }
  });

});